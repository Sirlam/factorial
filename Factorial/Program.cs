﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number to get its factorial: ");

            int number = Convert.ToInt32(Console.ReadLine());
            long fact = myFactorial(number);

            Console.WriteLine("The factorial of {0} is {1}", number, fact);
            Console.WriteLine("Press enter to exit");
            Console.ReadKey();
        }

        private static long myFactorial(int number)
        {
            if (number == 0)
            {
                return 1;
            }
            else
            {
                return number * myFactorial(number - 1);
            }
        }

    }
}
